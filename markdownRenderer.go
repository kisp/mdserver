package mdserver

import (
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/renderer/html"

	"github.com/yuin/goldmark"
)

type Markdown goldmark.Markdown

var (
	markdown Markdown
)

func CreateOrGetMarkdownRenderer() Markdown {
	if markdown == nil {
		markdown = goldmark.New(
			goldmark.WithRendererOptions(html.WithUnsafe()),
			goldmark.WithExtensions(
				extension.Table,
				extension.TaskList,
				extension.Strikethrough,
				extension.Linkify,
				extension.DefinitionList,
				extension.Footnote,
			),
		)
	}

	return markdown
}
