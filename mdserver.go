package mdserver

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func AddHttpHandler() {
	http.HandleFunc("/", httpMarkdownHandler())
}

func Listen(port string) {
	fmt.Println("Server started on :" + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func httpMarkdownHandler() http.HandlerFunc {
	markdown := CreateOrGetMarkdownRenderer()
	collection := CreateOrGetIndexer()

	return func(w http.ResponseWriter, r *http.Request) {
		defer CatchErrors(w)

		parts := sanitizeURL(r.URL.Path)

		var fileError error
		var fileContent []byte
		if len(parts) == 1 {
			fileName := strings.Replace(parts[0], "+", " ", -1)
			fmt.Printf("Accessing %v\n", fileName)
			fileContent, fileError = ioutil.ReadFile(fileName)
			if fileError == nil && !hasExtension(fileName, ".md") {
				w.WriteHeader(200)
				w.Write(fileContent)
				return
			}
		}
		// rendering markdown
		if len(parts) > 1 || fileError != nil {
			fileContent = []byte(renderMatchesIntoMd(collection, parts))
		}
		var outputBuf bytes.Buffer
		fmt.Fprintln(&outputBuf, "<head>"+
			"<link rel=\"stylesheet\" href=\"index.css\">"+
			"<link rel=\"stylesheet\" href=\"/main.css\"></head>")

		if err := markdown.Convert(fileContent, &outputBuf); err == nil {
			w.WriteHeader(200)
			w.Write(outputBuf.Bytes())
		} else {
			panic(ConvertError)
		}
	}
}
