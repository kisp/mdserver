package mdserver

import (
	"fmt"
	"io/ioutil"
	"strings"
	"sync"
	"time"
	"unsafe"
)

type IFileContentIndexer interface {
	Process(file File)
	FindFiles(word string) []int
	FindMatches(word string) []Match
	Invalidate(file File)
}

type Index struct {
	matches  map[string][]Match
	mu       sync.Mutex
	commands chan IndexingCommand
}

func NewIndex() *Index {
	ix := &Index{
		matches:  map[string][]Match{},
		commands: make(chan IndexingCommand),
	}
	ix.printStats(30 * time.Second)
	go ix.processCommands()
	return ix
}

const ADD = 0
const REMOVE = 1

type IndexingCommand struct {
	commandType int
	file        File
}

func (ix *Index) Process(file File) {
	ix.commands <- IndexingCommand{ADD, file}
}

func (ix *Index) Invalidate(file File) {
	ix.commands <- IndexingCommand{REMOVE, file}
}

func (ix *Index) processCommands() {
	for cmd := range ix.commands {
		switch cmd.commandType {
		case ADD:
			ix.process(cmd.file)
			break
		case REMOVE:
			ix.invalidate(cmd.file.id)
			break
		}
	}
}

func (ix *Index) printStats(duration time.Duration) {
	ticker := time.NewTicker(duration)

	var oMatches, oSize, oIxlen int

	go func() {
		for range ticker.C {
			matches := 0
			size := 0

			ix.mu.Lock()
			ixLen := len(ix.matches)
			for k, v := range ix.matches {
				size += int(unsafe.Sizeof(k))
				size += int(unsafe.Sizeof(v))
				matches += len(v)
			}
			ix.mu.Unlock()

			if oMatches != matches || oSize != size || oIxlen != ixLen {
				fmt.Printf("Index contains %d words %d matches in total of %d bytes\n",
					ixLen,
					matches,
					size)
				oMatches = matches
				oSize = size
				oIxlen = ixLen
			}
		}
	}()
}

// can called async
func (ix *Index) get(phrase string) []Match {
	ix.mu.Lock()
	found := ix.matches[phrase]
	ret := make([]Match, len(found))
	copy(ret, found)
	ix.mu.Unlock()
	return ret
}

// FindFiles returns distinct file indices
func (ix *Index) FindFiles(word string) []int {

	m := map[int]bool{}

	// make distinct
	for _, match := range ix.get(word) {
		m[match.file] = true
	}

	var ret []int
	for k, _ := range m {
		ret = append(ret, k)
	}

	return ret
}

// FindMatches returns Matches of word
func (ix *Index) FindMatches(word string) []Match {
	return ix.get(word)
}

// can called async
func (ix *Index) process(file File) {
	if fileContent, fileError := ioutil.ReadFile(file.name); fileError == nil {
		content := string(fileContent)

		// we lock once / file
		tmp := make(map[string][]Match)

		for lineNumber, line := range strings.Split(content, "\n") {
			cleanLine := sanitizeChars(line)

			for _, key := range strings.Fields(cleanLine) {
				tmp[key] = append(tmp[key], NewMatch(file.id, lineNumber))
			}
		}

		ix.mu.Lock()
		for key, matches := range tmp {
			ix.matches[key] = append(ix.matches[key], matches...)
		}
		ix.mu.Unlock()
	}
}

var NonIndexFriendlyRunes = []rune(",;.!?*|-+@#$%^&*()[]-_`~<>/\\:'\"")

func sanitizeChars(line string) string {
	ret := []rune(line)
outer:
	for i := 0; i < len(ret); i++ {
		for j := 0; j < len(NonIndexFriendlyRunes); j++ {
			if ret[i] == NonIndexFriendlyRunes[j] {
				ret[i] = ' '
				continue outer
			}
		}
	}
	return strings.ToLower(string(ret))
}

// Drop matches in file with fileIndex
func (ix *Index) invalidate(fileIndex int) {
	ix.mu.Lock()
	for key, matches := range ix.matches {
		var validMatches []Match

		for _, match := range matches {
			if match.file != fileIndex {
				validMatches = append(validMatches, match)
			}
		}
		ix.matches[key] = validMatches
	}
	ix.mu.Unlock()
}
