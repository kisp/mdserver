module bitbucket.org/kisp/mdserver

go 1.15

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/yuin/goldmark v1.3.6
)
