package mdserver

import (
	"fmt"
	"strings"
)

var SafeRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-/+ ")

func sanitizeURL(input string) []string {
	lookFor := input
	runes := []rune(lookFor)
	lookFor = string(runes[1:])

	dots := 0

	// log invalid
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Invalid path: %v\n", err)
			panic(InvalidPath)
		}
	}()

examine:
	for _, v := range []rune(lookFor) {
		// count consecutive dots
		if v == '.' {
			if dots > 0 {
				panic(input)
			} else {
				dots++
			}
			continue examine
		} else {
			dots = 0
		}
		for _, a := range SafeRunes {
			if v == a {
				continue examine
			}
		}
		panic(input)
	}

	parts := splitter(lookFor)
	return parts
}

func splitter(str string) []string {
	return strings.Split(str, " ")
}
