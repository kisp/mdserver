package mdserver

type APIRequest struct {
	Parts []string
}

func (ar *APIRequest) fromJSON(json map[string]interface{}) {
	ar.Parts = json["parts"].([]string)
}
