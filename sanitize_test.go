package mdserver

import "testing"

func TestSanitize(t *testing.T) {

	// GIVEN
	nonValid := "/../../someFile"
	// WHEN
	str := sanitizeURL(nonValid)
	// THEN

	if str[0] != "../../someFile" {
		t.Error("Did not cut the first character!")
	}
}
