package mdserver

import "net/http"

var (
	InvalidPath    = "invalid path"
	ConvertError   = "convert error"
	InvalidRequest = "invalid request"
)

func CatchErrors(w http.ResponseWriter) {
	if err := recover(); err != nil {
		switch err {
		case ConvertError:
			w.WriteHeader(http.StatusUnprocessableEntity)
			w.Write([]byte("convert error"))
			break
		case InvalidPath:
			w.WriteHeader(http.StatusPreconditionFailed)
			w.Write([]byte("invalid"))
			break
		case InvalidRequest:
			w.WriteHeader(http.StatusNotAcceptable)
			w.Write([]byte("invalid request, please provide at least one queryParam \"part\""))
			break

		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("server error"))
		}
	}
}
