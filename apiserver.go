package mdserver

import (
	"encoding/json"
	"net/http"
)

func AddApiHandler() {
	http.HandleFunc("/search", apiSearchHandler())
}

func apiSearchHandler() http.HandlerFunc {
	collection := CreateOrGetIndexer()

	return func(w http.ResponseWriter, r *http.Request) {
		defer CatchErrors(w)

		var apiRequest APIRequest

		if parts, ok := r.URL.Query()["part"]; ok {
			apiRequest = APIRequest{Parts: parts}
		} else {
			panic(InvalidRequest)
		}

		filenameMatches, fileContentMatches := findMatches(collection, apiRequest.Parts)
		matches := collection.FindMatchingWords(apiRequest.Parts)

		data := map[string]interface{}{
			"nameMatches":    filenameMatches,
			"contentMatches": fileContentMatches,
			"matches":        matches,
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(data)
	}
}
