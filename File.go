package mdserver

import (
	"os"
	"strings"
	"time"
)

type File struct {
	name string
	// using index instead of pointer for smaller sizes
	id      int
	modTime time.Time
}

var fileSeqId int

func NewFile(path string, fileInfo os.FileInfo, indexer IFileContentIndexer) *File {
	fileSeqId += 1
	name := fileInfo.Name()
	f := File{
		id:      fileSeqId,
		name:    path,
		modTime: fileInfo.ModTime(),
	}
	if hasExtension(name, ".md") || hasExtension(name, ".txt") {
		go indexer.Process(f)
	}
	return &f
}

func (f File) Destroy(indexer IFileContentIndexer) {
	indexer.Invalidate(f)
}

func hasExtension(fileName string, extension string) bool {
	extensionLength := len(extension)
	return strings.LastIndex(fileName, extension) == len(fileName)-extensionLength
}
