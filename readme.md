# Hello visitor!

To **install** md-server please run

```shell
go get bitbucket.org/kisp/mdserver/cmd/md-server
```

* [Feature list](table.md)


Use it from the browser directly `http://localhost:8080`
or via the API:
`http://localhost:8080/search?part=<term1>[&part=term2...]`
