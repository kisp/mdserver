package mdserver

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

// StaticFileCollection represents a collection of served files
// files can be searched for matching tags and are updated via fsnotify events
// TODO build prefix index
type StaticFileCollection struct {
	files              []*File
	mu                 sync.Mutex
	rootPath           []string
	fileContentIndexer IFileContentIndexer
}

var singleIndexer *StaticFileCollection

// NewStaticFileCollection creates StaticFileCollection
// file collection has a single instance
func NewStaticFileCollection(ix IFileContentIndexer,
	pollTime time.Duration) *StaticFileCollection {
	if singleIndexer != nil {
		return singleIndexer
	}
	singleIndexer = &StaticFileCollection{
		files:              []*File{},
		fileContentIndexer: ix,
	}
	singleIndexer.pollFiles(pollTime)
	return singleIndexer
}

// CollectFiles start watching the path for changes with fsnotify + walk the dirs recursively
// watcher needs to be on the same thread, as using it
// so it is boundled here. This way CollectFiles can be called as
// a public API from a goFunc
func (sfc *StaticFileCollection) CollectFiles(rootPath string) {
	sfc.rootPath = append(sfc.rootPath, rootPath)
	go sfc.pollLoop()
}

func (sfc *StaticFileCollection) walkFiles() {
	for _, path := range sfc.rootPath {
		if err := filepath.Walk(path, sfc.visitDir); err != nil {
			log.Fatal("Error searching files")
		}
	}
}

/// FindMatchingFilenames returns filenames matched by criteria
func (sfc *StaticFileCollection) FindMatchingFilenames(parts []string) []string {
	var matchedFiles []string
	sfc.mu.Lock()
nextFile:
	for _, file := range sfc.files {
		pathLower := strings.ToLower(file.name)
		for _, tag := range parts {
			tagLower := strings.ToLower(tag)
			if strings.LastIndex(pathLower, tagLower) == -1 {
				continue nextFile
			}
		}
		matchedFiles = append(matchedFiles, replaceBackSlash(file.name))
	}
	sfc.mu.Unlock()

	return matchedFiles
}

// FindFilesByMatchingWords returns file name matches where the content matched all the parts
func (sfc *StaticFileCollection) FindFilesByMatchingWords(parts []string) []string {
	var matchedFiles []string
	// intersect with map
	matches := map[string]int{}

	for _, tag := range parts {
		tagLower := strings.ToLower(tag)

		for _, fileIx := range sfc.fileContentIndexer.FindFiles(tagLower) {
			// TODO: log search for increasing ix
			for _, file := range sfc.files {
				if file.id == fileIx {
					matches[file.name] = matches[file.name] + 1
				}
			}
		}
	}

	for k, v := range matches {
		if v == len(parts) {
			matchedFiles = append(matchedFiles, replaceBackSlash(k))
		}
	}

	return matchedFiles
}

// FindMatchesByMatchingWords returns union of matches
func (sfc *StaticFileCollection) FindMatchingWords(parts []string) []RenderedMatch {
	var matches []RenderedMatch

	for _, tag := range parts {
		tagLower := strings.ToLower(tag)

		for _, m := range sfc.fileContentIndexer.FindMatches(tagLower) {
			// TODO: log search for increasing ix
			for _, file := range sfc.files {
				if file.id == m.file {
					matches = append(matches, RenderedMatch{
						File: file.name,
						Line: m.line,
					})
				}
			}
		}
	}

	return matches
}

func (sfc *StaticFileCollection) visitDir(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}
	if info.IsDir() {
		if strings.LastIndex(info.Name(), "node_modules") > -1 {
			return filepath.SkipDir
		}
		if strings.LastIndex(info.Name(), ".git") > -1 {
			return filepath.SkipDir
		}
	} else {
		sfc.handleFileChange("CHANGE", info, path)
	}
	return nil
}

func (sfc *StaticFileCollection) handleFileChange(operation string, fileInfo os.FileInfo, path string) {
	changeStatusForPrint := ""
	filename := path //fileInfo.Name()
	switch operation {
	case "GC":
		sfc.mu.Lock()
		if ix := indexOf(&sfc.files, filename); ix > -1 {
			file := sfc.files[ix]
			go file.Destroy(sfc.fileContentIndexer)
			sfc.files = append(sfc.files[:ix], sfc.files[ix+1:]...)
			changeStatusForPrint = fmt.Sprintf("-%d", file.id)
		}
		sfc.mu.Unlock()
		break
	case "CHANGE":
		sfc.mu.Lock()
		if ix := indexOf(&sfc.files, filename); ix == -1 {
			// new file
			newFile := NewFile(filename, fileInfo, sfc.fileContentIndexer)
			sfc.files = append(sfc.files, newFile)
			changeStatusForPrint = fmt.Sprintf("+%d", newFile.id)
		} else if ix > -1 {
			// existing
			file := sfc.files[ix]
			// check filechange
			if fileInfo.ModTime() != file.modTime {
				file.Destroy(sfc.fileContentIndexer)
				newFile := NewFile(filename, fileInfo, sfc.fileContentIndexer)

				sfc.files[ix] = newFile
				changeStatusForPrint = fmt.Sprintf("%d->%d", file.id, newFile.id)
			}
		}
		sfc.mu.Unlock()
		break
	default:
		changeStatusForPrint = "??" + operation
	}

	if changeStatusForPrint != "" {
		fmt.Printf("%s : %s Total: %d files.\n", changeStatusForPrint, filename, len(sfc.files))
	}
}

func (sfc *StaticFileCollection) pollFiles(duration time.Duration) {
	ticker := time.NewTicker(duration)

	fmt.Printf("File checking interval set to %s.\n", duration)

	go func() {
		for range ticker.C {
			sfc.pollLoop()
		}
	}()

}

var oFiles int

func (sfc *StaticFileCollection) pollLoop() {
	sfc.walkFiles()

	for _, v := range sfc.files {
		if info, err := os.Stat(v.name); err != nil {
			// find relativePath from first valid rootPath
		relPathLoop:
			for _, rootPath := range sfc.rootPath {
				if relativePath, err := filepath.Rel(rootPath, v.name); err == nil {
					sfc.handleFileChange("GC", info, relativePath)
					break relPathLoop
				}
			}
		}
	}

	fileCount := len(sfc.files)

	if fileCount != oFiles {
		fmt.Printf("%d valid files exist in register.\n", fileCount)
		oFiles = fileCount
	}
}

func indexOf(arr *[]*File, fileName string) int {
	for i, v := range *arr {
		if v.name == fileName {
			return i
		}
	}
	return -1
}

func replaceBackSlash(filename string) string {
	return strings.Replace(filename, "\\", "/", -1)
}
