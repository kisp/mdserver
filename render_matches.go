package mdserver

import (
	"fmt"
	"strings"
)

func findMatches(collection *StaticFileCollection, parts []string) (filenameMatches []string, fileContentMatches []string) {
	filenameMatches = collection.FindMatchingFilenames(parts)
	fileContentMatches = collection.FindFilesByMatchingWords(parts)

	return
}

// TODO implement ranking
func renderMatchesIntoMd(collection *StaticFileCollection, parts []string) string {
	fmt.Printf("Looking for tags %v\n", parts)

	matchedFiles, fileContentMatches := findMatches(collection, parts)
	source := ""
	var ix int
	for i, v := range matchedFiles {
		ix = i + 1
		link := strings.Replace(v, " ", "+", -1)
		source += fmt.Sprintf("* %d [%s](/%s)\n", i+1, v, link)
	}

	contentSource := ""
	for j, v := range fileContentMatches {
		link := strings.Replace(v, " ", "+", -1)
		contentSource += fmt.Sprintf("* %d [%s](/%s)\n", j+ix+1, v, link)
	}

	source = fmt.Sprintf(
		"# %d Total matches\n"+
			"## %d Filename matches\n%s\n"+
			"## %d File content matches\n%s"+
			"\n",
		len(matchedFiles)+len(fileContentMatches),
		len(matchedFiles),
		source,
		len(fileContentMatches),
		contentSource)

	return source
}
