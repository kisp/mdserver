package main

import (
	mdserver "bitbucket.org/kisp/mdserver"
)

func main() {
	ix := mdserver.CreateOrGetIndexer()

	go ix.CollectFiles(".")

	mdserver.AddApiHandler()
	mdserver.AddHttpHandler()
	mdserver.Listen("8080")
}
