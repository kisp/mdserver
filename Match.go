package mdserver

type Match struct {
	file int
	line int
}

func NewMatch(fileIx int, lineNumber int) Match {
	return Match{
		file: fileIx,
		line: lineNumber,
	}
}

type RenderedMatch struct {
	File string
	Line int
}
