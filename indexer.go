package mdserver

import "time"

var (
	index      *Index
	collection *StaticFileCollection
)

func CreateOrGetIndexer() *StaticFileCollection {
	if index == nil {
		index = NewIndex()
	}
	if collection == nil {
		collection = NewStaticFileCollection(index, 60*time.Second)
	}
	return collection
}

func init() {
	CreateOrGetIndexer()
}
