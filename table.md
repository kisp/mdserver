# Features

## Functional requirements

Are tracked here...

Status |Functionality                                                  |
-----  |:--------------------------------------------------------------|
Breakdown> | Edit functionality
WIP    | get api for search results
WIP    | PUT api for updating file content
Done   | Replace fsnotify with polling - because it is buggy on windows
Done   | Renaming a directory -> not watching?
Done   | Can not remove Parent directory as nested directory watching locks it - fsevents bug?
TODO   | Improve match results on content file content match w/ displaying context
TODO   | Introduce match relevance
TODO   | Support serve with header Content-Type / support MIME TYPES
Breakdown | Serve a special editor app with a prefix or suffix
Breakdown | Serve not only mark-downs (SVGs are broken)
FIXED  | Moving out directories does not work on windows
FIXED  | A file can be added second time by moving the directory back! the matches show up two times!
Done   | Provide a JSON API on /search?part=<searchterm> and-point
Done   | Fix indexing - leaking matches?
Done   | Search in file content / introduce indexing "words" -> Matches; currently displaying filenames only
Done   | Render.md file
Done   | Search by file name / tags
Done   | Search in a case insensitive way
Done   | Ensure access files with space in the filename on Linux
Done   | Ensure access files with space in the filename on Windows
Done   | Watch files for changes
Done   | Ability for styling, introduce global /main.css link
Done   | Ability for styling 2, introduce folder local index.css link

## Non-functional requirements

| Status | What | Affects |
| -------- | ------ | --- |
| | code cleanup basic functions | Development |
| | code coverage for basic functions | Development |
| TODO | Upfront pre-indexing / improve search in filenames | Performance |
